+++
title = "Home"
menu = "main"
+++

Hey there! My name is **Alex**, known as **Zeue** online, welcome to my website! If you're here, you either want to know more about me or just clicked on random links like a mad man.

On the off-chance that you're here to learn more about me, I hope you're ready to be disappointed! Because I haven't finished this website yet, _oof_, but don't fear as I have some more information down below. Be sure to also check out my social medias and code repositories in the bottom-right.

<hr/>

<h4><b>Technologies</b></h4>
<p class="h4 leftAlign">
  <a href="https://kubernetes.io/" target="_blank" class="badge monoFont badge-primary" data-toggle="tooltip" data-placement="top" title="Production-Grade Container Orchestration">
    Kubernetes
  </a>
  <a href="https://docker.io/" target="_blank" class="badge monoFont badge-primary" data-toggle="tooltip" data-placement="top" title="Alternative to Kubernetes">
    Docker
  </a>
  <a href="https://jenkins.io/" target="_blank" class="badge monoFont badge-primary" data-toggle="tooltip" data-placement="top" title="Leading Open Source Automation Server">
    Jenkins
  </a>
  <a href="https://www.mysql.com/" target="_blank" class="badge monoFont badge-primary" data-toggle="tooltip" data-placement="top" title="Open-Source Relational Database">
    MySQL
  </a>
  <a href="https://www.mongodb.com/" target="_blank" class="badge monoFont badge-primary" data-toggle="tooltip" data-placement="top" title="Document-Oriented Database">
    MongoDB
  </a>
  <a href="https://redis.io/" target="_blank" class="badge monoFont badge-primary" data-toggle="tooltip" data-placement="top" title="In-Memory Data Structure Store">
    Redis
  </a>
</p>

<h4><b>Concepts</b></h4>
<p class="h4 leftAlign">
  <a href="https://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/19860014876.pdf" target="_blank" class="badge monoFont badge-success" data-toggle="tooltip" data-placement="top" title="Production-Grade Container Orchestration">
    Load Balancing
    <span class="badge monoFont badge-light">
      L4 & L7
    </span>
  </a>
  <a href="https://www.atlassian.com/continuous-delivery/ci-vs-ci-vs-cd" target="_blank" class="badge monoFont badge-success" data-toggle="tooltip" data-placement="top" title="Continuous Integration/Delivery">
    CI/CD
  </a>
  <a href="#" target="_blank" class="badge monoFont badge-success" data-toggle="tooltip" data-placement="top" title="Continuous Integration/Delivery">
    Frameworks
  </a>
  <a href="#" target="_blank" class="badge monoFont badge-success" data-toggle="tooltip" data-placement="top" title="How Modern Apps/Services Connect Together">
    APIs
    <span class="badge monoFont badge-light">
      RAW & RESTful
    </span>
  </a>
  <span class="badge monoFont badge-success" data-toggle="tooltip" data-placement="top" title="Version Control Systems">
    VCS
    <span class="badge monoFont badge-light">
      Git & SVN
    </span>
  </span>
</p>

<h4><b>Languages && Data Formats</b></h4>
<p class="h4 leftAlign">
  <span class="badge monoFont badge-warning">
    PHP
    <span class="badge monoFont badge-light">
      7+
    </span>
  </span>
  <span class="badge monoFont badge-warning">
    JavaScript
    <span class="badge monoFont badge-light">
      ES6
    </span>
  </span>
  <a href="https://golang.org/" target="_blank" class="badge monoFont badge-warning" data-toggle="tooltip" data-placement="top" title="Powers this website!">
    Go
    <span class="badge monoFont badge-light">
      1.9+
    </span>
  </a>
  <a href="https://www.ruby-lang.org/" target="_blank" class="badge monoFont badge-warning">
    Ruby
    <span class="badge monoFont badge-light">
      2+
    </span>
  </a>
  <a href="https://www.python.org/"" target="_blank" class="badge monoFont badge-warning">
    Python
    <span class="badge monoFont badge-light">
      2.7 & 3+
    </span>
  </a>
  <span class="badge monoFont badge-warning">
    Coffeescript
  </span>
  <span class="badge monoFont badge-warning">
    HTML
  </span>
  <span class="badge monoFont badge-warning">
    CSS
  </span>
  <span class="badge monoFont badge-warning">
    LESS
  </span>
  <span class="badge monoFont badge-warning">
    SASS
  </span>
  <span class="badge monoFont badge-warning">
    C
  </span>
  <span class="badge monoFont badge-warning">
    C++
  </span>
  <span class="badge monoFont badge-warning">
    C#/.NET
  </span>
  <span class="badge monoFont badge-warning">
    Java
  </span>
  <span class="badge monoFont badge-warning">
    JSON
  </span>
  <span class="badge monoFont badge-warning">
    XML
  </span>
  <span class="badge monoFont badge-warning">
    CSV
  </span>
</p>

<h4><b>Frameworks && Libraries</b></h4>
<p class="h4 leftAlign">
  <a href="https://symfony.com/" target="_blank" class="badge monoFont badge-danger">
    Symfony
    <span class="badge monoFont badge-light">
      4.1+
    </span>
  </a>
  <a href="https://laravel.com/" target="_blank" class="badge monoFont badge-danger">
    Laravel
    <span class="badge monoFont badge-light">
      5.7+
    </span>
  </a>
  <a href="https://reactjs.org/" target="_blank" class="badge monoFont badge-danger">
    React
    <span class="badge monoFont badge-light">
      15+
    </span>
  </a>
  <a href="https://jquery.com/" target="_blank" class="badge monoFont badge-danger">
    jQuery
  </a>
  <a href="https://underscorejs.org/" target="_blank" class="badge monoFont badge-danger">
    UnderscoreJS
  </a>
  <a href="https://wordpress.org/" target="_blank" class="badge monoFont badge-danger">
    WordPress
  </a>
  <a href="https://vuejs.org/" target="_blank" class="badge monoFont badge-danger">
    Vue
    <span class="badge monoFont badge-light">
      2+
    </span>
  </a>
  <a href="https://angular.io/" target="_blank" class="badge monoFont badge-danger">
    Angular
    <span class="badge monoFont badge-light">
      2+
    </span>
  </a>
  <a href="https://www.emberjs.com/" target="_blank" class="badge monoFont badge-danger">
    Ember
    <span class="badge monoFont badge-light">
      3+
    </span>
  </a>
  <a href="https://www.meteor.com/" target="_blank" class="badge monoFont badge-danger">
    Meteor
  </a>
  <a href="https://foundation.zurb.com/" target="_blank" class="badge monoFont badge-danger">
    Foundation
    <span class="badge monoFont badge-light">
      5+
    </span>
  </a>
  <a href="https://nodejs.org/" target="_blank" class="badge monoFont badge-danger">
    NodeJS
  </a>
  <a href="https://rubyonrails.org/" target="_blank" class="badge monoFont badge-danger">
    Ruby on Rails
  </a>
  <a href="https://getbootstrap.com/" target="_blank" class="badge monoFont badge-danger">
    BootStrap
    <span class="badge monoFont badge-light">
      4+
    </span>
  </a>
  <a href="https://www.djangoproject.com/" target="_blank" class="badge monoFont badge-danger">
    Django
    <span class="badge monoFont badge-light">
      2+
    </span>
  </a>
  <a href="https://ionicframework.com/" target="_blank" class="badge monoFont badge-danger">
    Ionic
  </a>
  <a href="https://phonegap.com/" target="_blank" class="badge monoFont badge-danger">
    PhoneGap
  </a>
  <a href="https://www.drupal.org/" target="_blank" class="badge monoFont badge-danger">
    Drupal
  </a>
</p>
